# CMake generated Testfile for 
# Source directory: /home/cassis/ros_ws/src
# Build directory: /home/cassis/ros_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(Cassis_Baxter_Example)
SUBDIRS(golf_example)
SUBDIRS(moveit_robots/atlas_moveit_config)
SUBDIRS(baxter_common/baxter_common)
SUBDIRS(baxter_common/baxter_description)
SUBDIRS(baxter/baxter_sdk)
SUBDIRS(moveit_robots/iri_wam_moveit_config)
SUBDIRS(moveit_robots/moveit_robots)
SUBDIRS(moveit_robots/r2_moveit_generated)
SUBDIRS(baxter_common/rethink_ee_description)
SUBDIRS(moveit_robots/baxter/baxter_moveit_config)
SUBDIRS(baxter_common/baxter_maintenance_msgs)
SUBDIRS(baxter_common/baxter_core_msgs)
SUBDIRS(baxter_interface)
SUBDIRS(baxter_tools)
SUBDIRS(moveit_robots/baxter/baxter_ikfast_left_arm_plugin)
SUBDIRS(moveit_robots/baxter/baxter_ikfast_right_arm_plugin)
SUBDIRS(moveit_robots/atlas_v3_moveit_config)
SUBDIRS(vicon_bridge)
SUBDIRS(baxter_examples)
