#!/usr/bin/env python

#####################################################################################
#  2018/06/03       Cassis                                                          #
#  First try how to let baxter pick up the goods without Vicon			    #
#####################################################################################

import rospy
import baxter_interface
import sys
import argparse
import struct
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from std_msgs.msg import Header
from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)


def move(right_limb,left_limb):
	rospy.init_node('Hello_Baxter')
	rns = "ExternalTools/" + right_limb + "/PositionKinematicsNode/IKService"
	lns = "ExternalTools/" + left_limb + "/PositionKinematicsNode/IKService"
	riksvc = rospy.ServiceProxy(rns, SolvePositionIK)
	liksvc = rospy.ServiceProxy(lns, SolvePositionIK)
	ikreq = SolvePositionIKRequest()
	hdr = Header(stamp=rospy.Time.now(), frame_id='base')
	
	#current_limb_dict = limb_interface.joint_angles()
	#current_pose = limb_interface.endpoint_pose()

	limb_right = baxter_interface.Limb('right')
	limb_left = baxter_interface.Limb('left')

	angles_right = limb_right.joint_angles()
	angles_left = limb_left.joint_angles()

	#print angles_right
	#print angles_left

	#right IK
	poses1 = {
		'right': PoseStamped(
			header=hdr,
			pose=Pose(
				position=Point(
					x=0.656982770038,
					y=4.852598021641,
					z=0.0388609422173,
				),
				orientation=Quaternion(
					x=0.367048116303,
					y=0.885911751787,
					z=-0.108908281936,
					w=0.261868353356,
				),
			),
		),
	}
	
	ikreq.pose_stamp.append(poses1)
        print ikreq	
	print "\n------------------\n"
	poses2 = {
		'right': PoseStamped(
			header=hdr,
			pose=Pose(
				position=Point(
					x=0.656982770038,
					y=0.852598021641,
					z=0.0388609422173,
				),
				orientation=Quaternion(
					x=0.367048116303,
					y=0.885911751787,
					z=0.108908281936,
					w=0.261868353356,
				),
			),
		),
	}
	ikreq.pose_stamp.append(poses2)
	print ikreq

	try:
    		rospy.wait_for_service(rns, 5.0)
		rospy.wait_for_service(lns, 5.0)
    		rresp = riksvc(ikreq)
    		lresp = liksvc(ikreq)
	except (rospy.ServiceException, rospy.ROSException), e:
    		rospy.logerr("Service call failed: %s" % (e,))
    		return 1
	# Check if result valid, and type of seed ultimately used to get solution
	# convert rospy's string representation of uint8[]'s to int's
	rresp_seeds = struct.unpack('<%dB' % len(rresp.result_type),
                               rresp.result_type)
	print rresp_seeds
	if (rresp_seeds[0] != rresp.RESULT_INVALID):
    		seed_str = {
                		ikreq.SEED_USER: 'User Provided Seed',
                		ikreq.SEED_CURRENT: 'Current Joint Angles',
                		ikreq.SEED_NS_MAP: 'Nullspace Setpoints',
               		}.get(rresp_seeds[0], 'None')
    		print("SUCCESS - Valid Joint Solution Found from Seed Type: %s" %
           		(seed_str,))
    		# Format solution into Limb API-compatible dictionary
    		joint_poses1 = dict(zip(rresp.joints[0].name, rresp.joints[0].position))
   		#pose3 = dict(zip(rresp.joints[0].name, rresp.joints[0].position))
    		print "\nIK Joint Solution:\n", limb_poses1
    		print "------------------"
    		print "Response Message:\n", rresp
	else:
    		print("INVALID POSE - No Valid Joint Solution Found.")

	# Check if result valid, and type of seed ultimately used to get solution
	# convert rospy's string representation of uint8[]'s to int's
	lresp_seeds = struct.unpack('<%dB' % len(lresp.result_type),
                               lresp.result_type)
	print lresp_seeds
	if (lresp_seeds[0] != lresp.RESULT_INVALID):
    		seed_str = {
                		ikreq.SEED_USER: 'User Provided Seed',
                		ikreq.SEED_CURRENT: 'Current Joint Angles',
                		ikreq.SEED_NS_MAP: 'Nullspace Setpoints',
               		}.get(lresp_seeds[0], 'None')
    		print("SUCCESS - Valid Joint Solution Found from Seed Type: %s" %
           		(seed_str,))
    		# Format solution into Limb API-compatible dictionary
    		joint_poses1 = dict(zip(lresp.joints[0].name, lresp.joints[0].position))
   		#pose3 = dict(zip(lresp.joints[0].name, lresp.joints[0].position))
    		print "\nIK Joint Solution:\n", limb_poses1
    		print "------------------"
    		print "Response Message:\n", lresp
	else:
    		print("INVALID POSE - No Valid Joint Solution Found.")

	#gripper
	right_gripper = baxter_interface.Gripper('right')
	left_gripper = baxter_interface.Gripper('left')

	for _move in range(1):  				
		#check the right gripper open or not
    		if right_gripper == "close":
         		right_gripper.open()   
    		else:
         		right_gripper.open()   
    	
		#right   
    		limb_right.move_to_joint_positions(joint_poses1)
    		limb_right.move_to_joint_positions(joint_poses2)
   		#limb_right.move_to_joint_positions(ikreq.pose_stamp.append(pose3))
    	#right_gripper.close()    
    	#limb_right.move_to_joint_positions(wave_3)
    	#limb_right.set_joint_position_speed(speed=0.18)
    	#limb_right.move_to_joint_positions(wave_4)
    	#right_gripper.open(timeout=1)
    	#limb_right.set_joint_position_speed(speed=0.18)
    	#limb_right.move_to_joint_positions(wave_3)     
    	#limb_right.move_to_joint_positions(wave_5)
    	
	#check the left gripper open or not
    		if left_gripper == "close":
         		left_gripper.open()   
    		else:
         		left_gripper.open()   
    	#left
    	#limb_left.move_to_joint_positions(wave_6)
    	#limb_left.move_to_joint_positions(wave_7)
    	#left_gripper.close()    
    	#limb_left.move_to_joint_positions(wave_8)
    	#limb_left.set_joint_position_speed(speed=0.18)
    	#limb_left.move_to_joint_positions(wave_9)
    	#left_gripper.open(timeout=1)
    	#limb_left.set_joint_position_speed(speed=0.18)
    	#limb_left.move_to_joint_positions(wave_8)     
	#limb_left.move_to_joint_positions(wave_10)
	return 0



if __name__ == '__main__':
	move('right_limb','left_limb')


