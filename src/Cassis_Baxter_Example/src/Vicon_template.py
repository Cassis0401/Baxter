#!/usr/bin/env python

import time
import rospy
import numpy as np
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import TransformStamped


def callback(transform):
	f = open("/home/cassis/ros_ws/src/Cassis_Baxter_Example/srctarget_points.txt","w+")
	points[0] = transform.transform.translation.x
	points[1] = transform.transform.translation.y
	points[2] = transform.transform.translation.z
	print points
	f.write("%f		%f		%f \n" % (points[0],points[1],points[2]))



def listener():
	
	rospy.init_node('Vicon_Listener', anonymous=True) # node
	rospy.Subscriber('/vicon/Baxter/Baxter', TransformStamped, callback) # topic
	rospy.spin()
	

if __name__ == '__main__':
	
	
	points = np.array([0.0,0.0,0.0])
	listener()
	f.close() 

