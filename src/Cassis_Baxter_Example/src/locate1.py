#!/usr/bin/env python

#####################################################################################
#  2018/06/03       Cassis                                                          #
#  First try how to let baxter pick up the goods without Vicon			    #
#####################################################################################

import rospy
import baxter_interface
import sys
import argparse
import struct
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from std_msgs.msg import Header
from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)


def move(limb):
	rospy.init_node('Hello_Baxter')
	ns = "ExternalTools/" + limb + "/PositionKinematicsNode/IKService"
	iksvc = rospy.ServiceProxy(ns, SolvePositionIK)
	ikreq = SolvePositionIKRequest()
	hdr = Header(stamp=rospy.Time.now(), frame_id='base')
	current_limb_dict = limb_interface.joint_angles()
	current_pose = limb_interface.endpoint_pose()

	limb_right = baxter_interface.Limb('right')
	limb_left = baxter_interface.Limb('left')

	angles_right = limb_right.joint_angles()
	angles_left = limb_left.joint_angles()

	print angles_right
	print angles_left

	angles_right['right_s0']=0.0
	angles_right['right_s1']=0.0
	angles_right['right_e0']=0.0
	angles_right['right_e1']=0.0
	angles_right['right_w0']=0.0
	angles_right['right_w1']=0.0
	angles_right['right_w2']=0.0

	angles_left['left_s0']=0.0
	angles_left['left_s1']=0.0
	angles_left['left_e0']=0.0
	angles_left['left_e1']=0.0
	angles_left['left_w0']=0.0
	angles_left['left_w1']=0.0
	angles_left['left_w2']=0.0

	print angles_right
	print angles_left

	limb_right.move_to_joint_positions(angles_right)
	limb_left.move_to_joint_positions(angles_left)

	#right IK
	pose1 = PoseStamped()
	pose1.pose.position.x=0.657579481614
	pose1.pose.position.y=0.851981417433
	pose1.pose.position.z=0.0388352386502
	pose1.pose.orientation.w=0.262162481772
	ikreq.pose_stamp.append(pose1)
	
	pose2 = PoseStamped()
	pose2.pose.position.x=0.5
	pose2.pose.position.y=0.8
	pose2.pose.position.z=0.0
	pose2.pose.orientation.w=0.0
	ikreq.pose_stamp.append(pose2)
    	
	print ikreq

	try:
    		rospy.wait_for_service(ns, 5.0)
    		resp = iksvc(ikreq)
	except (rospy.ServiceException, rospy.ROSException), e:
    		rospy.logerr("Service call failed: %s" % (e,))
    		return 1
	# Check if result valid, and type of seed ultimately used to get solution
	# convert rospy's string representation of uint8[]'s to int's
	resp_seeds = struct.unpack('<%dB' % len(resp.result_type),
                               resp.result_type)
	print resp_seeds
	if (resp_seeds[0] != resp.RESULT_INVALID):
    		seed_str = {
                		ikreq.SEED_USER: 'User Provided Seed',
                		ikreq.SEED_CURRENT: 'Current Joint Angles',
                		ikreq.SEED_NS_MAP: 'Nullspace Setpoints',
               		}.get(resp_seeds[0], 'None')
    		print("SUCCESS - Valid Joint Solution Found from Seed Type: %s" %
           		(seed_str,))
    		# Format solution into Limb API-compatible dictionary
    		pose1 = dict(zip(resp.joints[0].name, resp.joints[0].position))
    		pose2 = dict(zip(resp.joints[0].name, resp.joints[0].position))
    		print "\nIK Joint Solution:\n", limb_joints
    		print "------------------"
    		print "Response Message:\n", resp
	else:
    		print("INVALID POSE - No Valid Joint Solution Found.")
    	#limb_interface.move_to_joint_positions(pose1)


	#gripper
	right_gripper = baxter_interface.Gripper('right')
	left_gripper = baxter_interface.Gripper('left')
	
	for _move in range(1):  		
		#check the right gripper open or not
    		if right_gripper == "close":
         		right_gripper.open()   
    		else:
         		right_gripper.open()   
    	
		#right   
    		limb_right.move_to_joint_positions(pose1)
    		limb_right.move_to_joint_positions(pose2)
    	#right_gripper.close()    
    	#limb_right.move_to_joint_positions(wave_3)
    	#limb_right.set_joint_position_speed(speed=0.18)
    	#limb_right.move_to_joint_positions(wave_4)
    	#right_gripper.open(timeout=1)
    	#limb_right.set_joint_position_speed(speed=0.18)
    	#limb_right.move_to_joint_positions(wave_3)     
    	#limb_right.move_to_joint_positions(wave_5)
    	
	#check the left gripper open or not
    		if left_gripper == "close":
         		left_gripper.open()   
    		else:
         		left_gripper.open()   
    	#left
    	#limb_left.move_to_joint_positions(wave_6)
    	#limb_left.move_to_joint_positions(wave_7)
    	#left_gripper.close()    
    	#limb_left.move_to_joint_positions(wave_8)
    	#limb_left.set_joint_position_speed(speed=0.18)
    	#limb_left.move_to_joint_positions(wave_9)
    	#left_gripper.open(timeout=1)
    	#limb_left.set_joint_position_speed(speed=0.18)
    	#limb_left.move_to_joint_positions(wave_8)     
    	#limb_left.move_to_joint_positions(wave_10)
#arg_fmt = argparse.RawDescriptionHelpFormatter
#parser = argparse.ArgumentParser(formatter_class=arg_fmt,
                                     #description=main.__doc__)

#args = parser.parse_args(rospy.myargv()[1:])

sys.exit(move())


