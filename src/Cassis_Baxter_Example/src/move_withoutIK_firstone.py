#!/usr/bin/env python

#####################################################################################
#  2018/06/03       Cassis                                                          #
#  First try how to let baxter pick up the goods without Vicon			    #
#####################################################################################

import rospy
import baxter_interface

rospy.init_node('Hello_Baxter')

limb_right = baxter_interface.Limb('right')
limb_left = baxter_interface.Limb('left')

angles_right = limb_right.joint_angles()
angles_left = limb_left.joint_angles()

print angles_right
print angles_left

angles_right['right_s0']=0.0
angles_right['right_s1']=0.0
angles_right['right_e0']=0.0
angles_right['right_e1']=0.0
angles_right['right_w0']=0.0
angles_right['right_w1']=0.0
angles_right['right_w2']=0.0

angles_left['left_s0']=0.0
angles_left['left_s1']=0.0
angles_left['left_e0']=0.0
angles_left['left_e1']=0.0
angles_left['left_w0']=0.0
angles_left['left_w1']=0.0
angles_left['left_w2']=0.0

print angles_right
print angles_left

limb_right.move_to_joint_positions(angles_right)
limb_left.move_to_joint_positions(angles_left)

#right
wave_1 = {'right_s0': 0.5349757997750381, 'right_s1': -0.9403302229737587, 'right_e0': 0.7102331047909466,'right_e1':1.4154807720212654,'right_w0': -0.4237621926533455, 'right_w1': 1.2785729867024924,'right_w2':-2.7776557116635128}

wave_2 = {'right_s0': 0.915786530367592, 'right_s1': -0.5219369630780121, 'right_e0': 0.23201459416766884,'right_e1':1.4289031039152629,'right_w0': -0.5019952128355016, 'right_w1': 0.7148350471546028,'right_w2':-2.6614566669811928}

wave_3 = {'right_s0': 0.9303593478525034, 'right_s1': -0.9081166264281649, 'right_e0': 0.21629129109184334,'right_e1':1.0845244170349875,'right_w0': -0.24045148850103862, 'right_w1': 1.4081943632788099,'right_w2':-2.6614566669811928}

wave_4 = {'right_s0': 1.2363885150356435, 'right_s1': -0.38157772098649667, 'right_e0': 0.12003399665203363,'right_e1':1.0618982004136777,'right_w0': -0.16643691548556738, 'right_w1': 0.8341020534126937,'right_w2':-2.6614566669811928}

wave_5 = {'right_s0': 0.0, 'right_s1': 0.0, 'right_e0': 0.0,'right_e1':0.0,'right_w0': 0.0, 'right_w1': 0.0,'right_w2':0.0}

#left
wave_6 = {'left_s0': -1.1297768502776073, 'left_s1': -0.7125340759727747, 'left_e0': 0.46249520754745227,'left_e1':1.0128108152013444,'left_w0': -0.3451456772742181, 'left_w1': 1.3817331946877864,'left_w2':-0.17640779060682257}

wave_7 = {'left_s0': -1.088359369004701, 'left_s1': -0.4793689962141918, 'left_e0': 0.14035924209151535,'left_e1':1.259781722050896,'left_w0': -0.1829272089553356, 'left_w1': 0.8628641931855452,'left_w2':-0.31101460474376763}

wave_8 = {'left_s0': -1.3702283387786458, 'left_s1': -0.578310757032801, 'left_e0': 0.4084223847744914,'left_e1':0.9828981898375788,'left_w0': -0.44332044769888457, 'left_w1': 1.1688933603686853,'left_w2':-0.32482043183473636}

wave_9 = {'left_s0': -1.3679273675968178, 'left_s1': -0.507747640790072, 'left_e0': 0.13537380453088776,'left_e1':1.386718632248414,'left_w0': -0.42452918304728826, 'left_w1': 0.5192524966992126,'left_w2':-0.37160684586524145}

wave_10 = {'left_s0': 0.0, 'left_s1': 0.0, 'left_e0': 0.0,'left_e1':0.0,'left_w0': 0.0, 'left_w1': 0.0,'left_w2':0.0}

right_gripper = baxter_interface.Gripper('right')
left_gripper = baxter_interface.Gripper('left')



for _move in range(1):
    
    if right_gripper == "close":
         right_gripper.open()   
    else:
         right_gripper.open()   
    #right   
    limb_right.move_to_joint_positions(wave_1)
    limb_right.move_to_joint_positions(wave_2)
    right_gripper.close()    
    limb_right.move_to_joint_positions(wave_3)
    limb_right.set_joint_position_speed(speed=0.18)
    limb_right.move_to_joint_positions(wave_4)
    right_gripper.open(timeout=1)
    limb_right.set_joint_position_speed(speed=0.18)
    limb_right.move_to_joint_positions(wave_3)     
    limb_right.move_to_joint_positions(wave_5)
    
    if left_gripper == "close":
         left_gripper.open()   
    else:
         left_gripper.open()   
    #left
    limb_left.move_to_joint_positions(wave_6)
    limb_left.move_to_joint_positions(wave_7)
    left_gripper.close()    
    limb_left.move_to_joint_positions(wave_8)
    limb_left.set_joint_position_speed(speed=0.18)
    limb_left.move_to_joint_positions(wave_9)
    left_gripper.open(timeout=1)
    limb_left.set_joint_position_speed(speed=0.18)
    limb_left.move_to_joint_positions(wave_8)     
    limb_left.move_to_joint_positions(wave_10)

quit()

